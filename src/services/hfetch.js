import axios from 'axios';

const baseURL = process.env.APP_API_HOST;
const requestTimeout = 10000;

const instance = axios.create({
    baseURL,
    timeout: requestTimeout,
});

instance.interceptors.request.use((config) => {
    // config.headers['X-Client-Id'] = 'mini-app';
    return config;
}, (error) => Promise.reject(error));

// eslint-disable-next-line max-len
instance.interceptors.response.use((res) => {
    console.log('res', res.data);
    return Promise.resolve(res.data);
}, (error) => Promise.reject(error));

export default instance;
